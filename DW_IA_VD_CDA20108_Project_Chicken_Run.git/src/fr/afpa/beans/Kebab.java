package fr.afpa.beans;

import java.util.Scanner;

public abstract class Kebab extends Volaille {
	
	private double poids;
	private double prixParKilo;
	private double poidsAbattage;

	public Kebab(int id, int nbMax, double poids, double prixParKilo, double poidsAbattage) {
		super(id, nbMax);
		this.poids = poids;
		this.prixParKilo = prixParKilo;
		this.poidsAbattage = poidsAbattage;
	}

	public Kebab() {
		
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public double getPrixParKilo() {
		return prixParKilo;
	}

	public void setPrixParKilo(double prixParKilo) {
		this.prixParKilo = prixParKilo;
	}

	public double getPoidsAbattage() {
		return poidsAbattage;
	}

	public void setPoidsAbattage(double poidsAbattage) {
		this.poidsAbattage = poidsAbattage;
	}

	public void modifierPoids() {
		GestionVolaille gesVol = new GestionVolaille();
		Scanner scan = new Scanner(System.in);
		double pesee = scan.nextDouble();
		int positionV = rechercherVolaille();
		if (positionV != -1) {
			if(gesVol.getListeVolaille()[positionV] instanceof Canard) {
				((Canard)gesVol.getListeVolaille()[positionV]).setPoids(pesee);
			}
			else if(gesVol.getListeVolaille()[positionV] instanceof Poulet) {
				((Poulet)gesVol.getListeVolaille()[positionV]).setPoids(pesee);
			}else {
				System.out.println("On ne mange pas les Paons !!!");
			}
		}else {
			System.out.println("Vollaille Introuvable");
		}
	}
	
	// Note DW , je pense que la methode modierPoidsAbattage serait mieux dans GestionVollaille
	public abstract void modifierPoidsAbattage();
	
	public abstract void modifierPrix();
	
	
	
	
	
	
	
}
