package fr.afpa.beans;

public class GestionVolaille {

	private Volaille[] listeVolaille;
	public final static int NB_MAX_VOLAILLE = 7;
	public final static int NB_MAX_CARNARD = 4;
	public final static int NB_MAX_POULET = 5;
	public final static int NB_MAX_PAON = 3;
	
	
	public GestionVolaille(Volaille[] listeVolaille) {
		this.listeVolaille = listeVolaille;
	}
	
	public GestionVolaille() {
		this.listeVolaille = new Volaille[NB_MAX_VOLAILLE];
	}

	public Volaille[] getListeVolaille() {
		return listeVolaille;
	}

	public void setListeVolaille(Volaille[] listeVolaille) {
		this.listeVolaille = listeVolaille;
	}
	

}
