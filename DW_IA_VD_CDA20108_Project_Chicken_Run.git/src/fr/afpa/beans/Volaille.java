package fr.afpa.beans;

import java.util.Scanner;

public abstract class Volaille {

	private static int id;
	private int nbMax;
	private int identifiant;
	
	public Volaille(int id, int nbMax) {
		super();
		this.identifiant = ++id;
		this.nbMax = nbMax;
	}
	
	public Volaille() {}

	public int getId() {
		return id;
	}

	public int getNbMax() {
		return nbMax;
	}

	public void setNbMax(int nbMax) {
		this.nbMax = nbMax;
	}
	
	public int getIdentifiant() {
		return identifiant;
	}
	
	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}
	
	public abstract void volailleEnMoins();
	
	public int rechercherVolaille() {
		GestionVolaille gesVol = new GestionVolaille();
		Scanner scan = new Scanner(System.in);
		System.out.println(" Quel est l'identifiant du volatile ? ");
		// id attribu� � une volaille d�s l'entr�e dans la ferme
		int idVol = scan.nextInt();
		for (int i = 0; i < gesVol.getListeVolaille().length; i++) {
			if(gesVol.getListeVolaille()[i] != null && gesVol.getListeVolaille()[i].getIdentifiant() == idVol) {
				return i;
			}	
		}
		return -1;
	}
	
}
