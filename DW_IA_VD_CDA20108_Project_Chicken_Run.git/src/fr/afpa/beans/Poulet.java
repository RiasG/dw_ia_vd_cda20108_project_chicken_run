package fr.afpa.beans;

public final class Poulet extends Kebab {
	
	private static int nbPoulet;

	

	public Poulet(int id, int nbMax, double poids, double prixParKilo, double poidsAbattage) {
		super(id, nbMax, poids, prixParKilo, poidsAbattage);
		nbPoulet++;
	}

	@Override
	public void modifierPoids() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifierPoidsAbattage() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifierPrix() {
		// TODO Auto-generated method stub

	}
	
	
	public static int getNbPoulet() {
		return nbPoulet;
	}

	public static void setNbPoulet(int nbPoulet) {
		Poulet.nbPoulet = nbPoulet;
	}

	@Override
	public void volailleEnMoins() {
		Poulet.nbPoulet--;
	}
}
