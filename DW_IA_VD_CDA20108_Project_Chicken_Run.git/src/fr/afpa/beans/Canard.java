package fr.afpa.beans;

import java.util.Scanner;

public final class Canard extends Kebab {

	private static int nbCanard;
	
	public Canard(int id, int nbMax, double poids, double prixParKilo, double poidsAbattage) {
		super(id, nbMax, poids, prixParKilo, poidsAbattage);
		nbCanard++;
	}

	@Override
	public void modifierPoidsAbattage() {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifierPrix() {
		// TODO Auto-generated method stub

	}
	
	
	public static int getNbCanard() {
		return nbCanard;
	}

	public static void setNbCanard(int nbCanard) {
		Canard.nbCanard = nbCanard;
	}



	@Override
	public void volailleEnMoins() {
		Canard.nbCanard--;
	}

}
